import { createApp } from 'vue'
import store from './store'
import App from './App.vue'

const App1 = createApp(App)

App1.use(store).mount('#app')
