import { createStore } from 'vuex';

export default createStore({

  state: {
    windowWidth: window.innerWidth
  }, mutations: {
    setWindowWidth (state, payload) {
      state.windowWidth = window.innerWidth;
    }
  }
});
