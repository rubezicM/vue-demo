import { fileURLToPath, URL } from 'url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'



// https://vitejs.dev/config/
export default defineConfig({
  css: {
    preprocessorOptions: {
      scss: {
        additionalData (source, fp) {
          // All scss files ending with imports.scss
          // will not re-import additionalData
          if (fp.endsWith('import.scss')) return source;
          // Use additionalData from legacy nuxt scss options
          return `@import "@/assets/scss/_variables-import.scss"; ${source}`
        }
      }
    }
  },
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
